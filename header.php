		<!DOCTYPE html>
		<html lang="en">
		<!-- begin::Head -->

		<head>
		<meta charset="utf-8" />
		<title>Mybiz</title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">

		<!--begin::Web font -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
		WebFont.load({
		google: { "families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"] },
		active: function () {
		sessionStorage.fonts = true;
		}
		});
		</script>

		<!--end::Web font -->

		<!--begin::Global Theme Styles -->
		<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
		<!--RTL version:<link href="assets/vendors/base/vendors.bundle.rtl.css" rel="stylesheet" type="text/css" />-->

		<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />
		<!--RTL version:<link href="assets/demo/default/base/style.bundle.rtl.css" rel="stylesheet" type="text/css" />-->

		<!--end::Global Theme Styles -->

		<!--begin::Page Vendors Styles -->
		<link href="assets/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />

		<!--RTL version:<link href="assets/vendors/custom/fullcalendar/fullcalendar.bundle.rtl.css" rel="stylesheet" type="text/css" />-->

		<!--end::Page Vendors Styles -->
		<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />
		</head>

		<!-- end::Head -->

		<!-- begin::Body -->

		<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

		<!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">

		<!-- BEGIN: Header -->
		<header id="m_header" class="m-grid__item    m-header " m-minimize-offset="200" m-minimize-mobile-offset="200">
		<div class="m-container m-container--fluid m-container--full-height">
		<div class="m-stack m-stack--ver m-stack--desktop">

		<!-- BEGIN: Brand -->
		<div class="m-stack__item m-brand  m-brand--skin-dark ">
		<div class="m-stack m-stack--ver m-stack--general">
		<div style="width:80%;" class="m-stack__item m-stack__item--middle m-brand__logo">
		<a href="index.php" style="color:#fff; text-decoration:none;" class="m-brand__logo-wrapper">
		<!-- <img alt="" src="assets/demo/default/media/img/logo/logo_default_dark.png" /> -->
		<h5 style="margin-bottom:0px;">MYBIZ BY STARTBIZ</h5>
		</a>
		</div>
		<div class="m-stack__item m-stack__item--middle m-brand__tools">

		<!-- BEGIN: Left Aside Minimize Toggle -->
		<a href="javascript:;" id="m_aside_left_minimize_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block  ">
		<span></span>
		</a>

		<!-- END -->

		<!-- BEGIN: Responsive Aside Left Menu Toggler -->
		<a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
		<span></span>
		</a>
		<!-- END -->


		<!-- BEGIN: Topbar Toggler -->
		<a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
		<i class="flaticon-more"></i>
		</a>

		<!-- BEGIN: Topbar Toggler -->
		</div>
		</div>
		</div>

		<!-- END: Brand -->
		<div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">


		<!-- BEGIN: Topbar -->
		<div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general m-stack--fluid">

		<div class="m-stack__item m-topbar__nav-wrapper">
		<ul class="m-topbar__nav m-nav m-nav--inline">
		<li class="m-nav__item m-dropdown m-dropdown--large m-dropdown--arrow m-dropdown--align-center m-dropdown--mobile-full-width m-dropdown--skin-light	m-list-search m-list-search--skin-light"
		m-dropdown-toggle="click" id="m_quicksearch" m-quicksearch-mode="dropdown" m-dropdown-persistent="1">
		<a href="#" class="m-nav__link m-dropdown__toggle">
		<span class="m-nav__link-icon"><i class="flaticon-search-1"></i></span>
		</a>
		<div class="m-dropdown__wrapper">
		<span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
		<div class="m-dropdown__inner ">
		<div class="m-dropdown__header">
		<form class="m-list-search__form">
		<div class="m-list-search__form-wrapper">
		<span class="m-list-search__form-input-wrapper">
		<input id="m_quicksearch_input" autocomplete="off" type="text" name="q" class="m-list-search__form-input" value="" placeholder="Search...">
		</span>
		<span class="m-list-search__form-icon-close" id="m_quicksearch_close">
		<i class="la la-remove"></i>
		</span>
		</div>
		</form>
		</div>
		<div class="m-dropdown__body">
		<div class="m-dropdown__scrollable m-scrollable" data-scrollable="true" data-height="300" data-mobile-height="200">
		<div class="m-dropdown__content">
		</div>
		</div>
		</div>
		</div>
		</div>
		</li>
		 
		
		
		<li class="m-nav__item m-topbar__user-profile m-topbar__user-profile--img  m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light"
		m-dropdown-toggle="click">
		<a href="#" class="m-nav__link m-dropdown__toggle">
		<span class="m-topbar__userpic">
		<img src="assets/app/media/img/users/user4.jpg" class="m--img-rounded m--marginless" alt="" />
		</span>
		<span class="m-topbar__username m--hide">Nick</span>
		</a>
		<div class="m-dropdown__wrapper">
		<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
		<div class="m-dropdown__inner">
		<div class="m-dropdown__header m--align-center" style="background: url(assets/app/media/img/misc/user_profile_bg.jpg); background-size: cover;">
		<div class="m-card-user m-card-user--skin-dark">
		<div class="m-card-user__pic">
		<img src="assets/app/media/img/users/user4.jpg" class="m--img-rounded m--marginless" alt="" />
		</div>
		<div class="m-card-user__details">
		<span class="m-card-user__name m--font-weight-500">Mark Andre</span>
		<a href="" class="m-card-user__email m--font-weight-300 m-link">mark.andre@gmail.com</a>
		</div>
		</div>
		</div>
		<div class="m-dropdown__body">
		<div class="m-dropdown__content">
		<ul class="m-nav m-nav--skin-light">
		
		 
		 
		<li class="m-nav__item">
		<a href="header/profile.php" class="m-nav__link">
		<i class="m-nav__link-icon flaticon-chat-1"></i>
		<span class="m-nav__link-text">Profile Picture</span>
		</a>
		</li>
		<li class="m-nav__separator m-nav__separator--fit">
		</li>
		<li class="m-nav__item">
		<a href="header/profile.php" class="m-nav__link">
		<i class="m-nav__link-icon flaticon-info"></i>
		<span class="m-nav__link-text">Change password</span>
		</a>
		</li>
		<li class="m-nav__item">
		<a href="header/profile.php" class="m-nav__link">
		<i class="m-nav__link-icon flaticon-lifebuoy"></i>
		<span class="m-nav__link-text">Help & Support</span>
		</a>
		</li>
		<li class="m-nav__separator m-nav__separator--fit">
		</li>
		<li class="m-nav__item">
		<a href="login.php" class="btn m-btn--pill    btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder">Logout</a>
		</li>
		</ul>
		</div>
		</div>
		</div>
		</div>
		</li>
		</ul>
		</div>
		</div>

		<!-- END: Topbar -->
		</div>
		</div>
		</div>
		</header>
		<!-- END: Header -->


		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">


		<!-- BEGIN: Left Aside -->
		<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i class="la la-close"></i></button>

		<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">

		<!-- BEGIN: Aside Menu -->
		<div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">
		<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
		<li class="m-menu__item  m-menu__item--active" aria-haspopup="true"><a href="index.php" class="m-menu__link "><i class="m-menu__link-icon flaticon-cogwheel"></i><span class="m-menu__link-title"> <span class="m-menu__link-wrap"> <span class="m-menu__link-text">Dashboard</span>
		<span class="m-menu__link-badge"><span class="m-badge m-badge--danger">2</span></span> </span></span></a></li>
		<li class="m-menu__section ">
		<h4 class="m-menu__section-text">AUDIENCES</h4>
		<i class="m-menu__section-icon flaticon-more-v2"></i>
		</li>
		<li class="m-menu__item" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="visitors.php" class="m-menu__link"><i class="m-menu__link-icon flaticon-users"></i><span class="m-menu__link-text">Visitors</span></a>
		</li>

		<li class="m-menu__section ">
		<h4 class="m-menu__section-text">SEARCH ENGINE OPTIMIZATION</h4>
		<i class="m-menu__section-icon flaticon-more-v2"></i>
		</li>
		<li class="m-menu__item" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="keywords.php" class="m-menu__link"><i class="m-menu__link-icon flaticon-line-graph"></i><span class="m-menu__link-text">Keywords Ranking</span></a>
		</li>

		<li class="m-menu__section ">
		<h4 class="m-menu__section-text">SOCIAL NETWORK</h4>
		<i class="m-menu__section-icon flaticon-more-v2"></i>
		</li>
		<li class="m-menu__item" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link"><i class="m-menu__link-icon flaticon-facebook-letter-logo
		"></i><span class="m-menu__link-text">Facebook</span></a>
		</li>


		</ul>
		</div>

		<!-- END: Aside Menu -->
		</div>
		<!-- END: Left Aside -->