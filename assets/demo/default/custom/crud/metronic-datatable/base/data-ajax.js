var DatatableRemoteAjaxDemo = {
  init: function() {
    var t;
    (t = $(".m_datatable").mDatatable({
      data: {
        type: "remote",
        source: {
          read: {
            url:
              "https://keenthemes.com/metronic/themes/themes/metronic/dist/preview/inc/api/datatables/demos/default.php",
            map: function(t) {
              var e = t;
              return void 0 !== t.data && (e = t.data), e;
            }
          }
        },
        pageSize: 10,
        serverPaging: !0,
        serverFiltering: !0,
        serverSorting: !0
      },
      layout: { scroll: !1, footer: !1 },
      sortable: !0,
      pagination: !0,
      toolbar: {
        items: { pagination: { pageSizeSelect: [10, 20, 30, 50, 100] } }
      },
      search: { input: $("#generalSearch") },
      columns: [
        {
          field: "RecordID",
          title: "#",
          sortable: !1,
          width: 40,
          selector: !1,
          textAlign: "center"
        },
        {
          field: "Keywords",
          title: "Keywords",
          filterable: !1,
          width: 150,
          template: "{{OrderID}} - {{ShipCountry}}"
        },
        {
          field: "OrderID",
          title: "URL",
          filterable: !1,
          width: 150,
          template: "{{OrderID}} - {{ShipCountry}}"
        },
        {
          field: "ShipCountry",
          title: "Volumn de rech.",
          attr: { nowrap: "nowrap" },
          width: 150,
          template: function(t) {
            return t.ShipCountry + " - " + t.ShipCity;
          }
        },
        { field: "ShipCity", title: "Competition" },
        { field: "Currency", title: "Offre Suggeree", width: 100 },
        {
          field: "ShipDate",
          title: "Jan-7",
          type: "date",
          format: "MM/DD/YYYY"
        },
        { field: "Latitude", title: "Jan-8", type: "number" }
      ]
    })),
      $("#m_form_status").on("change", function() {
        t.search($(this).val(), "Status");
      }),
      $("#m_form_type").on("change", function() {
        t.search($(this).val(), "Type");
      }),
      $("#m_form_status, #m_form_type").selectpicker();
  }
};
jQuery(document).ready(function() {
  DatatableRemoteAjaxDemo.init();
});
