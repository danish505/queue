<!-- START INCLUDE HEADER -->
<?php include 'header.php';?>
<!-- END INCLUDE HEADER -->

<!-- START CONTENT -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">

	<!-- BEGIN: Subheader -->
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title ">REPORT</h3>
			</div>
			<div>
				<span class="m-subheader__daterange" id="m_dashboard_daterangepicker">
					<span class="m-subheader__daterange-label">
						<span class="m-subheader__daterange-title"></span>
						<span class="m-subheader__daterange-date m--font-brand"></span>
					</span>
					<a href="#" class="btn btn-sm btn-brand m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
						<i class="la la-angle-down"></i>
					</a>
				</span>
			</div>
		</div>
	</div>

	<div class="m-content">
		<div class="row">
			<div class="col-xl-12">

				<!--begin:: Widgets/Quick Stats-->
				<div class="row m-row--full-height">
					<div class="col-sm-12 col-md-4 col-lg-4">
						<div class="m-portlet m-portlet--border-bottom-brand" style="margin-bottom:0;">
							<div class="m-portlet__body">
								<div class="m-widget26">
									<div class="m-widget26__number">
										570
										<small>VISITORS</small>
									</div>
									<div class="m-widget26__chart" style="height:90px; ">
										<div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
											<div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
												<div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
											</div>
											<div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
												<div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
											</div>
										</div>
										<canvas id="m_chart_quick_stats_1" width="220" height="110" class="chartjs-render-monitor" style="display: block;  height: 110px;"></canvas>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-sm-12 col-md-4 col-lg-4">
						<div class="m-portlet m-portlet--border-bottom-success" style="margin-bottom:0;">
							<div class="m-portlet__body">
								<div class="m-widget26">
									<div class="m-widget26__number">
										230
										<small>AVERAGE POSITION</small>
									</div>
									<div class="m-widget26__chart" style="height:90px; ">
										<div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
											<div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
												<div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
											</div>
											<div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
												<div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
											</div>
										</div>
										<canvas id="m_chart_quick_stats_2" width="220" height="110" class="chartjs-render-monitor" style="display: block;  height: 110px;"></canvas>
									</div>
								</div>
							</div>
						</div>

					</div>

					<div class="col-sm-12 col-md-4 col-lg-4">
						<div class="m-portlet m-portlet--border-bottom-success" style="margin-bottom:0;">
							<div class="m-portlet__body">
								<div class="m-widget26">
									<div class="m-widget26__number">
										230
										<small>CONTACT STATS</small>
									</div>
									<div class="m-widget26__chart" style="height:90px; ">
										<div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
											<div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
												<div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
											</div>
											<div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
												<div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
											</div>
										</div>
										<canvas id="m_chart_quick_stats_3" width="220" height="110" class="chartjs-render-monitor" style="display: block;  height: 110px;"></canvas>
									</div>
								</div>
							</div>
						</div>
					</div>


				</div>

				<!--end:: Widgets/Quick Stats-->
			</div>
		</div>
	</div>


	<!-- END: Subheader -->
	<div class="m-content">

		<!--Begin::Section-->
		<div class="m-portlet">
			<div class="m-portlet__body  m-portlet__body--no-padding">
				<div class="row m-row--no-padding m-row--col-separator-xl">

					<div class="col-xl-4">
						<!--begin:: Widgets/Stats2-1 -->
						<div class="m-widget1">
							<div class="m-widget1__item">
								<div class="row m-row--no-padding align-items-center">
									<div class="col">
										<h3 class="m-widget1__title">VISITORS</h3>
										<br>
									</div>
									<div class="col m--align-right">
										<span class="m-widget1__number m--font-brand">+$10%</span>
										<br>
										<br>
									</div>
								</div>
							</div>
							<div class="m-widget1__item">
								<div class="row m-row--no-padding align-items-center">
									<div class="col">
										<br>
										<h3 class="m-widget1__title">AVERAGE POSITION</h3>
										<br>
									</div>
									<div class="col m--align-right">
										<br>
										<span class="m-widget1__number m--font-danger">+25%</span>
										<br>
										<br>
									</div>
								</div>
							</div>
							<div class="m-widget1__item">
								<div class="row m-row--no-padding align-items-center">
									<div class="col">
										<br>
										<h3 class="m-widget1__title">CONTACT STATS</h3>
									</div>
									<div class="col m--align-right">
										<br>
										<span class="m-widget1__number m--font-success">+5%</span>
									</div>
								</div>
							</div>
						</div>
						<!--end:: Widgets/Stats2-1 -->
					</div>

					<div class="col-xl-4">
						<!--begin:: Widgets/Daily Sales-->
						<div class="m-widget14">
							<div class="m-widget14__header m--margin-bottom-30">
								<h3 class="m-widget14__title">
									DAILY LEAD
								</h3>
								<span class="m-widget14__desc">
									Check out each column for more details
								</span>
							</div>
							<div class="m-widget14__chart" style="height:120px;">
								<canvas id="m_chart_daily_sales"></canvas>
							</div>
						</div>
						<!--end:: Widgets/Daily Sales-->
					</div>

					<div class="col-xl-4">
						<!--begin:: Widgets/Profit Share-->
						<div class="m-widget14">
							<div class="m-widget14__header">
								<h3 class="m-widget14__title">
									TRAFFIC
								</h3>
								<span class="m-widget14__desc">
									Traffic Across The Web
								</span>
							</div>
							<div class="row  align-items-center">
								<div class="col">
									<div id="m_chart_profit_share" class="m-widget14__chart" style="height: 160px">
										<div class="m-widget14__stat">45</div>
									</div>
								</div>
								<div class="col">
									<div class="m-widget14__legends">
										<div class="m-widget14__legend">
											<span class="m-widget14__legend-text">60% Organic</span>
										</div>
										<div class="m-widget14__legend">
											<span class="m-widget14__legend-text">20% CPC</span>
										</div>
										<div class="m-widget14__legend">
											<span class="m-widget14__legend-text">+5% Referal</span>
										</div>
										<div class="m-widget14__legend">
											<span class="m-widget14__legend-text">+5% Social</span>
										</div>
										<div class="m-widget14__legend">
											<span class="m-widget14__legend-text">+5 Others</span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--end:: Widgets/Profit Share-->
					</div>

				</div>
			</div>
		</div>

		<!--End::Section-->

		<div class="m-portlet ">
			<div class="m-portlet__body  m-portlet__body--no-padding">
				<div class="row m-row--no-padding m-row--col-separator-xl">
					<div class="col-md-12 col-lg-6 col-xl-3">

						<!--begin::Total Profit-->
						<div class="m-widget24">
							<div class="m-widget24__item">
								<h4 class="m-widget24__title">
									CONVERSION
								</h4><br>
								<div class="m--space-10"></div>
								<div class="progress m-progress--sm">
									<div class="progress-bar m--bg-brand" role="progressbar" style="width: 78%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
								</div>

								<span class="m-widget24__number">
									78%
								</span>
							</div>
						</div>

						<!--end::Total Profit-->
					</div>
					<div class="col-md-12 col-lg-6 col-xl-3">

						<!--begin::New Feedbacks-->
						<div class="m-widget24">
							<div class="m-widget24__item">
								<h4 class="m-widget24__title">
									DOMAIL AUTHORITY
								</h4><br>
								<div class="m--space-10"></div>
								<div class="progress m-progress--sm">
									<div class="progress-bar m--bg-info" role="progressbar" style="width: 84%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
								</div>

								<span class="m-widget24__number">
									84%
								</span>
							</div>
						</div>

						<!--end::New Feedbacks-->
					</div>
					<div class="col-md-12 col-lg-6 col-xl-3">

						<!--begin::New Orders-->
						<div class="m-widget24">
							<div class="m-widget24__item">
								<h4 class="m-widget24__title">
									BACKLINKS
								</h4><br>
								<div class="m--space-10"></div>
								<div class="progress m-progress--sm">
									<div class="progress-bar m--bg-danger" role="progressbar" style="width: 69%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
								</div>

								<span class="m-widget24__number">
									69%
								</span>
							</div>
						</div>

						<!--end::New Orders-->
					</div>
					<div class="col-md-12 col-lg-6 col-xl-3">

						<!--begin::New Users-->
						<div class="m-widget24">
							<div class="m-widget24__item">
								<h4 class="m-widget24__title">
									PAGE INDEX
								</h4><br>
								<div class="m--space-10"></div>
								<div class="progress m-progress--sm">
									<div class="progress-bar m--bg-success" role="progressbar" style="width: 90%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
								</div>

								<span class="m-widget24__number">
									90%
								</span>
							</div>
						</div>

						<!--end::New Users-->
					</div>
				</div>
			</div>
		</div>


		<div class="row">

			<div class="col-xl-12">

				<!--begin:: Widgets/Adwords Stats-->
				<div class="m-portlet m-portlet--full-height m-portlet--skin-light m-portlet--fit  m-portlet--rounded">
					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<h3 class="m-portlet__head-text">
									Activity
								</h3>
							</div>
						</div>
						<div class="m-portlet__head-tools">
							<ul class="m-portlet__nav">
								<li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push"
								 m-dropdown-toggle="hover">
									<a href="#" class="m-portlet__nav-link m-dropdown__toggle dropdown-toggle btn btn--sm m-btn--pill btn-secondary m-btn m-btn--label-brand">
										Today
									</a>
									<div class="m-dropdown__wrapper">
										<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
										<div class="m-dropdown__inner">
											<div class="m-dropdown__body">
												<div class="m-dropdown__content">
													<ul class="m-nav">
														<li class="m-nav__section m-nav__section--first">
															<span class="m-nav__section-text">Quick Actions</span>
														</li>
														<li class="m-nav__item">
															<a href="" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-share"></i>
																<span class="m-nav__link-text">Activity</span>
															</a>
														</li>
														<li class="m-nav__item">
															<a href="" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-chat-1"></i>
																<span class="m-nav__link-text">Messages</span>
															</a>
														</li>
														<li class="m-nav__item">
															<a href="" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-info"></i>
																<span class="m-nav__link-text">FAQ</span>
															</a>
														</li>
														<li class="m-nav__item">
															<a href="" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-lifebuoy"></i>
																<span class="m-nav__link-text">Support</span>
															</a>
														</li>
														<li class="m-nav__separator m-nav__separator--fit">
														</li>
														<li class="m-nav__item">
															<a href="#" class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm">Cancel</a>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
					<div class="m-portlet__body">
						<div class="m-widget21" style="min-height: 420px">
							<div class="row">
								<div class="col">
									<div class="m-widget21__item m--pull-left">
										<span class="m-widget21__icon">
											<a href="#" class="btn btn-brand m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
												<i class="fa flaticon-users m--font-light"></i>
											</a>
										</span>
										<div class="m-widget21__info">
											<span class="m-widget21__title">
												VISITORS
											</span><br>

										</div>
									</div>
								</div>
								<div class="col m--align-left">
									<div class="m-widget21__item m--pull-right">
										<span class="m-widget21__icon">
											<a href="#" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
												<i class="fa flaticon-layers m--font-light m--font-light"></i>
											</a>
										</span>
										<div class="m-widget21__info">
											<span class="m-widget21__title">
												Leads
											</span><br>

										</div>
									</div>
								</div>
							</div>
							<div class="m-widget21__chart m-portlet-fit--sides" style="height:310px;">
								<div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
									<div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
										<div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
									</div>
									<div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
										<div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
									</div>
								</div>
								<canvas id="m_chart_adwords_stats" width="597" height="310" class="chartjs-render-monitor" style="display: block; width: 597px; height: 310px;"></canvas>
							</div>
						</div>
					</div>
				</div>

				<!--end:: Widgets/Adwords Stats-->
			</div>
		</div>


	</div>
</div>
<!-- END CONTENT -->

<!-- START INCLUDE FOOTER -->
<?php include 'footer.php';?>
<!-- END INCLUDE FOOTER -->