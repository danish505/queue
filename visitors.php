<!-- START INCLUDE HEADER -->
<?php include 'header.php';?>
<!-- END INCLUDE HEADER -->

<!-- START CONTENT -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <!-- BEGIN: Subheader -->
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto"><h3 class="m-subheader__title ">VISITORS</h3></div>
    </div>
  </div>

  <div class="m-content">
    <!--Begin::Section-->
    <div class="row">
      <div class="col-xl-6">
        <!--begin:: Widgets/Product Sales-->
        <div
          class="m-portlet m-portlet--bordered-semi m-portlet--space m-portlet--full-height  m-portlet--rounded"
          style="height: calc(100%);"
        >
          <div class="m-portlet__body" style="height: calc(100%);">
            <div class="m-widget25">
              <span
                class="m-widget25__price m--font-brand"
                style="font-size:60px;"
                >2000</span
              >
              <span class="m-widget25__desc"><h4>VISITS</h4></span>
              <div
                class="m-widget25--progress"
                style="padding-top: 35px; margin-top: 45px;"
              >
                <div class="m-widget25__progress" style="width: 55%;">
                  <span class="m-widget25__progress-number"> 63% </span>
                  <div class="m--space-10"></div>
                  <div class="progress m-progress--sm">
                    <div
                      class="progress-bar m--bg-danger"
                      role="progressbar"
                      style="width: 63%;"
                      aria-valuenow="50"
                      aria-valuemin="0"
                      aria-valuemax="100"
                    ></div>
                  </div>
                  <span class="m-widget25__progress-sub"> SEO </span>
                </div>

                <div class="m-widget25__progress" style="width: 55%;">
                  <span class="m-widget25__progress-number"> 39% </span>
                  <div class="m--space-10"></div>
                  <div class="progress m-progress--sm">
                    <div
                      class="progress-bar m--bg-accent"
                      role="progressbar"
                      style="width: 39%;"
                      aria-valuenow="50"
                      aria-valuemin="0"
                      aria-valuemax="100"
                    ></div>
                  </div>
                  <span class="m-widget25__progress-sub"> SOCIAL </span>
                </div>
              </div>

              <div
                class="m-widget25--progress"
                style="margin-top:35px; padding-top:25px;"
              >
                <div class="m-widget25__progress" style="width: 55%;">
                  <span class="m-widget25__progress-number"> 54% </span>
                  <div class="m--space-10"></div>
                  <div class="progress m-progress--sm">
                    <div
                      class="progress-bar m--bg-warning"
                      role="progressbar"
                      style="width: 54%;"
                      aria-valuenow="50"
                      aria-valuemin="0"
                      aria-valuemax="100"
                    ></div>
                  </div>
                  <span class="m-widget25__progress-sub"> CPC </span>
                </div>

                <div class="m-widget25__progress" style="width: 55%;">
                  <span class="m-widget25__progress-number"> 54% </span>
                  <div class="m--space-10"></div>
                  <div class="progress m-progress--sm">
                    <div
                      class="progress-bar m--bg-primary"
                      role="progressbar"
                      style="width: 54%;"
                      aria-valuenow="50"
                      aria-valuemin="0"
                      aria-valuemax="100"
                    ></div>
                  </div>
                  <span class="m-widget25__progress-sub"> REFERRAL </span>
                </div>
              </div>
            </div>
          </div>
        </div>

        <!--end:: Widgets/Product Sales-->
      </div>

      <div class="col-xl-6">
        <!--begin:: Widgets/Quick Stats-->
        <div class="row m-row--full-height">
          <div class="col-sm-12 col-md-6 col-lg-6">
            <div
              class="m-portlet m-portlet--border-bottom-brand"
              style="margin-bottom:0;"
            >
              <div class="m-portlet__body">
                <div class="m-widget26">
                  <div class="m-widget26__number">
                    570 <small>PAGES VIEWS</small>
                  </div>
                  <div class="m-widget26__chart" style="margin-bottom:0;">
                    <div
                      class="chartjs-size-monitor"
                      style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"
                    >
                      <div
                        class="chartjs-size-monitor-expand"
                        style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"
                      >
                        <div
                          style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"
                        ></div>
                      </div>
                      <div
                        class="chartjs-size-monitor-shrink"
                        style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"
                      >
                        <div
                          style="position:absolute;width:200%;height:200%;left:0; top:0"
                        ></div>
                      </div>
                    </div>
                    <canvas
                      id="m_chart_quick_stats_1"
                      width="220"
                      height="110"
                      class="chartjs-render-monitor"
                      style="display: block;  height: 110px;"
                    ></canvas>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-sm-12 col-md-6 col-lg-6">
            <div
              class="m-portlet m-portlet--border-bottom-success"
              style="margin-bottom:0;"
            >
              <div class="m-portlet__body">
                <div class="m-widget26">
                  <div class="m-widget26__number">230 <small>LEADS</small></div>
                  <div class="m-widget26__chart" style="margin-bottom:0;">
                    <div
                      class="chartjs-size-monitor"
                      style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"
                    >
                      <div
                        class="chartjs-size-monitor-expand"
                        style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"
                      >
                        <div
                          style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"
                        ></div>
                      </div>
                      <div
                        class="chartjs-size-monitor-shrink"
                        style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"
                      >
                        <div
                          style="position:absolute;width:200%;height:200%;left:0; top:0"
                        ></div>
                      </div>
                    </div>
                    <canvas
                      id="m_chart_quick_stats_2"
                      width="220"
                      height="110"
                      class="chartjs-render-monitor"
                      style="display: block;  height: 110px;"
                    ></canvas>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-sm-12 col-md-6 col-lg-6">
            <br /><br />
            <div
              class="m-portlet m-portlet--border-bottom-success"
              style="margin-bottom:0;"
            >
              <div class="m-portlet__body">
                <div class="m-widget26">
                  <div class="m-widget26__number">
                    2 min 30s <small>AVERAGE SESSION DURATION</small>
                  </div>
                  <div class="m-widget26__chart" style="margin-bottom:0;">
                    <div
                      class="chartjs-size-monitor"
                      style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"
                    >
                      <div
                        class="chartjs-size-monitor-expand"
                        style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"
                      >
                        <div
                          style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"
                        ></div>
                      </div>
                      <div
                        class="chartjs-size-monitor-shrink"
                        style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"
                      >
                        <div
                          style="position:absolute;width:200%;height:200%;left:0; top:0"
                        ></div>
                      </div>
                    </div>
                    <canvas
                      id="m_chart_quick_stats_3"
                      width="220"
                      height="110"
                      class="chartjs-render-monitor"
                      style="display: block;  height: 110px;"
                    ></canvas>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-sm-12 col-md-6 col-lg-6">
            <br />
            <br />
            <div
              class="m-portlet m-portlet--border-bottom-success"
              style="margin-bottom:0;"
            >
              <div class="m-portlet__body">
                <div class="m-widget26">
                  <div class="m-widget26__number">
                    80% <small>BOUNCE RATE</small>
                  </div>
                  <div class="m-widget26__chart" style="margin-bottom:0;">
                    <div
                      class="chartjs-size-monitor"
                      style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"
                    >
                      <div
                        class="chartjs-size-monitor-expand"
                        style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"
                      >
                        <div
                          style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"
                        ></div>
                      </div>
                      <div
                        class="chartjs-size-monitor-shrink"
                        style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"
                      >
                        <div
                          style="position:absolute;width:200%;height:200%;left:0; top:0"
                        ></div>
                      </div>
                    </div>
                    <canvas
                      id="m_chart_quick_stats_4"
                      width="220"
                      height="110"
                      class="chartjs-render-monitor"
                      style="display: block;  height: 110px;"
                    ></canvas>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <!--end:: Widgets/Quick Stats-->
      </div>
    </div>
    <!--End::Section-->
  </div>

  <div class="m-content">
    <div class="row">
      <div class="col-xl-6">
        <!--begin::Portlet-->
        <div class="m-portlet">
          <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
              <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">TOP VISIT</h3>
              </div>
            </div>
          </div>
          <div class="m-portlet__body">
            <!--begin::Section-->
            <div class="m-section" style="margin:0px">
              <div class="m-section__content" style="margin:0px">
                <div class="table-responsive">
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th></th>
                        <th>View</th>
                        <th class="m--font-brand">% Views</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Home</td>
                        <td>200</td>
                        <td class="m--font-brand">30%</td>
                      </tr>
                      <tr>
                        <td>About</td>
                        <td>50</td>
                        <td class="m--font-brand">10%</td>
                      </tr>
                      <tr>
                        <td>Contact</td>
                        <td>10</td>
                        <td class="m--font-brand">5%</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>

            <!--end::Section-->
          </div>

          <!--end::Form-->
        </div>
        <!--end::Portlet-->
      </div>
    </div>
  </div>
</div>
<!-- END CONTENT -->

<!-- START INCLUDE FOOTER -->
<?php include 'footer.php';?>
<!-- END INCLUDE FOOTER -->
