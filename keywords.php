<!-- START INCLUDE HEADER -->
<?php include 'header.php';?>
<!-- END INCLUDE HEADER -->

<!-- START CONTENT -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <!-- BEGIN: Subheader -->
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto"><h3 class="m-subheader__title ">KEYWORDS</h3></div>
      <div>
        <span class="m-subheader__daterange" id="m_dashboard_daterangepicker">
          <span class="m-subheader__daterange-label">
            <span class="m-subheader__daterange-title"></span>
            <span class="m-subheader__daterange-date m--font-brand"></span>
          </span>
          <a
            href="#"
            class="btn btn-sm btn-brand m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill"
          >
            <i class="la la-angle-down"></i>
          </a>
        </span>
      </div>
    </div>
  </div>

  <div class="m-content">
    <div class="row">
      <div class="col-xl-12">
        <!--begin:: Widgets/Quick Stats-->
        <div class="row m-row--full-height">
          <div class="col-sm-12 col-md-4 col-lg-4">
            <div
              class="m-portlet m-portlet--border-bottom-brand"
              style="margin-bottom:0;"
            >
              <div class="m-portlet__body">
                <div class="m-widget26">
                  <div class="m-widget26__number">20 <small>TOP 2</small></div>
                  <div class="m-widget26__chart" style="height:90px; ">
                    <div
                      class="chartjs-size-monitor"
                      style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"
                    >
                      <div
                        class="chartjs-size-monitor-expand"
                        style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"
                      >
                        <div
                          style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"
                        ></div>
                      </div>
                      <div
                        class="chartjs-size-monitor-shrink"
                        style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"
                      >
                        <div
                          style="position:absolute;width:200%;height:200%;left:0; top:0"
                        ></div>
                      </div>
                    </div>
                    <canvas
                      id="m_chart_quick_stats_1"
                      width="220"
                      height="110"
                      class="chartjs-render-monitor"
                      style="display: block;  height: 110px;"
                    ></canvas>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-sm-12 col-md-4 col-lg-4">
            <div
              class="m-portlet m-portlet--border-bottom-success"
              style="margin-bottom:0;"
            >
              <div class="m-portlet__body">
                <div class="m-widget26">
                  <div class="m-widget26__number">23 <small>TOP 10</small></div>
                  <div class="m-widget26__chart" style="height:90px; ">
                    <div
                      class="chartjs-size-monitor"
                      style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"
                    >
                      <div
                        class="chartjs-size-monitor-expand"
                        style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"
                      >
                        <div
                          style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"
                        ></div>
                      </div>
                      <div
                        class="chartjs-size-monitor-shrink"
                        style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"
                      >
                        <div
                          style="position:absolute;width:200%;height:200%;left:0; top:0"
                        ></div>
                      </div>
                    </div>
                    <canvas
                      id="m_chart_quick_stats_2"
                      width="220"
                      height="110"
                      class="chartjs-render-monitor"
                      style="display: block;  height: 110px;"
                    ></canvas>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-sm-12 col-md-4 col-lg-4">
            <div
              class="m-portlet m-portlet--border-bottom-success"
              style="margin-bottom:0;"
            >
              <div class="m-portlet__body">
                <div class="m-widget26">
                  <div class="m-widget26__number">
                    10 <small>TOP 100</small>
                  </div>
                  <div class="m-widget26__chart" style="height:90px; ">
                    <div
                      class="chartjs-size-monitor"
                      style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"
                    >
                      <div
                        class="chartjs-size-monitor-expand"
                        style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"
                      >
                        <div
                          style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"
                        ></div>
                      </div>
                      <div
                        class="chartjs-size-monitor-shrink"
                        style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"
                      >
                        <div
                          style="position:absolute;width:200%;height:200%;left:0; top:0"
                        ></div>
                      </div>
                    </div>
                    <canvas
                      id="m_chart_quick_stats_3"
                      width="220"
                      height="110"
                      class="chartjs-render-monitor"
                      style="display: block;  height: 110px;"
                    ></canvas>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <!--end:: Widgets/Quick Stats-->
      </div>
    </div>
  </div>

  <div class="m-content">
    <!--Begin::Section-->
    <div class="row">

      <div class="col-xl-4">
        <!--begin:: Widgets/Inbound Bandwidth-->
        <div
          class="m-portlet m-portlet--bordered-semi m-portlet--half-height m-portlet--fit "
          style="min-height: 300px"
        >
          <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
              <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">Average Position</h3>
              </div>
            </div>
          </div>
          <div class="m-portlet__body">
            <!--begin::Widget5-->
            <div class="m-widget20">
              <div class="m-widget20__number m--font-success">670</div>
              <div class="m-widget20__chart" style="height:160px;">
                <div
                  class="chartjs-size-monitor"
                  style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"
                >
                  <div
                    class="chartjs-size-monitor-expand"
                    style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"
                  >
                    <div
                      style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"
                    ></div>
                  </div>
                  <div
                    class="chartjs-size-monitor-shrink"
                    style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"
                  >
                    <div
                      style="position:absolute;width:200%;height:200%;left:0; top:0"
                    ></div>
                  </div>
                </div>
                <canvas
                  id="m_chart_bandwidth1"
                  width="376"
                  height="160"
                  class="chartjs-render-monitor"
                  style="display: block; width: 376px; height: 160px;"
                ></canvas>
              </div>
            </div>
            <!--end::Widget 5-->
          </div>
        </div>

        <!--end:: Widgets/Inbound Bandwidth-->
        <div class="m--space-30"></div>

        <!--begin:: Widgets/Outbound Bandwidth-->
        <div
          class="m-portlet m-portlet--bordered-semi m-portlet--half-height m-portlet--fit "
          style="min-height: 300px"
        >
          <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
              <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">Traffic Position</h3>
              </div>
            </div>
          </div>
          <div class="m-portlet__body">
            <!--begin::Widget5-->
            <div class="m-widget20">
              <div class="m-widget20__number m--font-warning">340</div>
              <div class="m-widget20__chart" style="height:160px;">
                <div
                  class="chartjs-size-monitor"
                  style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"
                >
                  <div
                    class="chartjs-size-monitor-expand"
                    style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"
                  >
                    <div
                      style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"
                    ></div>
                  </div>
                  <div
                    class="chartjs-size-monitor-shrink"
                    style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"
                  >
                    <div
                      style="position:absolute;width:200%;height:200%;left:0; top:0"
                    ></div>
                  </div>
                </div>
                <canvas
                  id="m_chart_bandwidth2"
                  width="376"
                  height="160"
                  class="chartjs-render-monitor"
                  style="display: block; width: 376px; height: 160px;"
                ></canvas>
              </div>
            </div>

            <!--end::Widget 5-->
          </div>
        </div>

        <!--end:: Widgets/Outbound Bandwidth-->
      </div>

      <div class="col-xl-8">
        <!--begin:: Widgets/Application Sales-->
        <div class="m-portlet m-portlet--full-height ">
         
          <div class="m-portlet__body">
            <div class="tab-content">
              <div class="tab-pane active" id="m_widget11_tab1_content">
                <!--begin::Widget 11-->
                <div class="m-widget11">
                  <div class="table-responsive">
                    <!--begin::Table-->
                    <table class="table">
                      <!--begin::Tbody-->
                      <tbody>

                        <tr>
                          <td>
                            <span class="m-widget11__title">Keyword on Top 3</span>
                          </td>
                          <td>
                            <div
                              class="m-widget11__chart"
                              style="height:50px; width: 100px"
                            >
                              <iframe
                                class="chartjs-hidden-iframe"
                                tabindex="-1"
                                style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"
                              ></iframe>
                              <canvas
                                id="m_chart_sales_by_apps_1_1"
                                style="display: block; width: 100px; height: 50px;"
                                width="100"
                                height="50"
                              ></canvas>
                            </div>
                          </td>
                          
                          <td class="m--align-right m--font-brand">20%</td>
						</tr>
						
                        <tr>
                          <td>
                            <span class="m-widget11__title">Keyword on Top 10</span>
                            
                          </td>
                          
                          <td>
                            <div
                              class="m-widget11__chart"
                              style="height:50px; width: 100px"
                            >
                              <iframe
                                class="chartjs-hidden-iframe"
                                tabindex="-1"
                                style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"
                              ></iframe>
                              <canvas
                                id="m_chart_sales_by_apps_1_2"
                                style="display: block; width: 100px; height: 50px;"
                                width="100"
                                height="50"
                              ></canvas>
                            </div>
                          </td>
                          <td class="m--align-right m--font-brand">40%</td>
						</tr>
						
                        <tr>
                          
                          <td>
                            <span class="m-widget11__title">Position Up</span>
                            
                          </td>
                         
                          <td>
                            <div
                              class="m-widget11__chart"
                              style="height:50px; width: 100px"
                            >
                              <iframe
                                class="chartjs-hidden-iframe"
                                tabindex="-1"
                                style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"
                              ></iframe>
                              <canvas
                                id="m_chart_sales_by_apps_1_3"
                                style="display: block; width: 100px; height: 50px;"
                                width="100"
                                height="50"
                              ></canvas>
                            </div>
                          </td>
                          <td class="m--align-right m--font-brand">10%</td>
						</tr>
						
                        <tr>
                          <td>
                            <span class="m-widget11__title">Position Down</span>
                            
                          </td>
                          <td>
                            <div class="m-widget11__chart" style="height:50px; width: 100px">
                              <iframe
                                class="chartjs-hidden-iframe"
                                tabindex="-1"
                                style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"
                              ></iframe>
                              <canvas
                                id="m_chart_sales_by_apps_1_4"
                                style="display: block; width: 100px; height: 50px;"
                                width="100"
                                height="50"
                              ></canvas>
                            </div>
                          </td>
                          
                          <td class="m--align-right m--font-brand">20%</td>
						</tr>
						
                      </tbody>

                      <!--end::Tbody-->
                    </table>

                    <!--end::Table-->
                  </div>
                  
                </div>

                <!--end::Widget 11-->
              </div>
            </div>
          </div>
        </div>

        <!--end:: Widgets/Application Sales-->
	  </div>
	  
    </div>
    <!--End::Section-->
  </div>

<div class="m-content">
		<!--Begin::Section-->
		<div class="row">
			
			<div class="col-xl-12 col-sm-12 col-md-12">

				<!--begin:: Widgets/Application Sales-->
				<div class="m-portlet m-portlet--full-height ">
					<div class="m-portlet__body">
						<div class="tab-content">
						<div class="tab-pane active show" id="m_widget11_tab2_content">

<!--begin::Widget 11-->
<div class="m-widget11">
	<div class="table-responsive">

		<!--begin::Table-->
		<table class="table">

			<!--begin::Thead-->
			<thead>
				<tr>
					<td class="m-widget11__app">Keyword</td>
					<td class="m-widget11__sales">Position</td>
					<td class="m-widget11__change">Stat</td>
					<td class="m-widget11__price">Url</td>
					<td class="m-widget11__total m--align-right">Volume</td>
				</tr>
			</thead>

			<!--end::Thead-->

			<!--begin::Tbody-->
			<tbody>

				<tr>
					<td>
						<span class="m-widget11__title">Web Agency</span>
					</td>
					<td>7</td>
					<td>
						<div class="m-widget11__chart" style="height:50px; width: 100px"><div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
							<iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>
							<canvas id="m_chart_sales_by_apps_2_1" style="display: block; width: 100px; height: 50px;" height="50" width="100" class="chartjs-render-monitor"></canvas>
						</div>
					</td>
					<td>
					<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="URL">
						<i class="la la-link"></i>                        
					</a>
					</td>
					<td class="m--align-right m--font-brand">2500</td>
				</tr>

				<tr>
					<td>
						<span class="m-widget11__title">Creative Agency</span>
					</td>
					<td>5</td>
					<td>
						<div class="m-widget11__chart" style="height:50px; width: 100px">
           <div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
                <div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
							<iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>
							<canvas id="m_chart_sales_by_apps_2_2" style="display: block; width: 100px; height: 50px;" height="50" width="100" class="chartjs-render-monitor"></canvas>
						</div>
					</td>
					<td>
						<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="URL">
							<i class="la la-link"></i>                        
						</a>
					</td>
					<td class="m--align-right m--font-brand">100</td>
				</tr>
				
				
			</tbody>

			<!--end::Tbody-->
		</table>

		<!--end::Table-->
	</div>
</div>

<!--end::Widget 11-->
</div>
						</div>
					</div>
				</div>

				<!--end:: Widgets/Application Sales-->
			</div>


		</div>
		<!--End::Section-->
</div>


</div>
<!-- END CONTENT -->

<!-- START INCLUDE FOOTER -->
<?php include 'footer.php';?>
<!-- END INCLUDE FOOTER -->
